<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (c) Techbee e.U.
  ~ All rights reserved. This program and the accompanying materials
  ~ are made available under the terms of the GNU Public License v3.0
  ~ which accompanies this distribution, and is available at
  ~ http://www.gnu.org/licenses/gpl.html
  -->

<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">



    <data>
        <import type="android.view.View" />
        <import type="at.techbee.jtx.util.DateTimeUtils" />

        <variable
            name="model"
            type="at.techbee.jtx.ui.IcalEditViewModel" />
    </data>


    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">


        <com.google.android.material.switchmaterial.SwitchMaterial
            android:id="@+id/edit_task_add_timezone_switch"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="8dp"
            android:layout_marginEnd="24dp"
            android:checked="@={model.addTimezoneTodoChecked}"
            android:enabled="@{model.addTimeChecked}"
            android:visibility="@{model.addStartedAndDueTimeVisible ? View.VISIBLE : View.GONE}"
            android:text="@string/add_timezone_switch"
            app:layout_constraintEnd_toStartOf="@id/edit_task_add_time_switch"
            app:layout_constraintHorizontal_bias="1.0"
            app:layout_constraintStart_toStartOf="@+id/edit_task_started_card"
            app:layout_constraintTop_toTopOf="parent"
            app:switchPadding="8dp" />


        <com.google.android.material.switchmaterial.SwitchMaterial
            android:id="@+id/edit_task_add_time_switch"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="8dp"
            android:checked="@={model.addTimeChecked}"
            android:visibility="@{model.addStartedAndDueTimeVisible ? View.VISIBLE : View.GONE}"
            android:text="@string/add_time_switch"
            app:layout_constraintEnd_toEndOf="@+id/edit_task_completed_card"
            app:layout_constraintTop_toTopOf="parent"
            app:switchPadding="8dp" />

        <com.google.android.material.textview.MaterialTextView
            android:id="@+id/edit_task_started_header"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/started"
            android:textAlignment="center"
            android:textStyle="bold"
            android:visibility="@{model.starteddateVisible ? View.VISIBLE : View.GONE}"
            app:layout_constraintEnd_toEndOf="@+id/edit_task_started_card"
            app:layout_constraintStart_toStartOf="@+id/edit_task_started_card"
            app:layout_constraintTop_toBottomOf="@id/edit_task_add_time_switch" />

        <com.google.android.material.card.MaterialCardView
            android:id="@+id/edit_task_started_card"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="16dp"
            android:foregroundGravity="center_horizontal"
            android:visibility="@{model.starteddateVisible ? View.VISIBLE : View.GONE}"
            app:flow_horizontalAlign="center"
            app:layout_constraintEnd_toStartOf="@+id/edit_task_due_card"
            app:layout_constraintHorizontal_bias="0.5"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/edit_task_started_header">

            <LinearLayout
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_horizontal"
                android:orientation="vertical"
                android:paddingStart="4dp"
                android:paddingEnd="4dp"
                android:paddingBottom="8dp">

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_started_day"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.convertLongToDayString(model.iCalObjectUpdated.dtstart, model.iCalObjectUpdated.dtstartTimezone)}"
                    android:textAlignment="center"
                    android:textSize="36sp"
                    android:textStyle="bold"
                    android:visibility="@{model.iCalObjectUpdated.dtstart != null ? View.VISIBLE : View.GONE}"
                    tools:text="31" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_started_month"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.convertLongToMonthString(model.iCalObjectUpdated.dtstart, model.iCalObjectUpdated.dtstartTimezone)}"
                    android:textAlignment="center"
                    android:textSize="12sp"
                    android:textStyle="bold"
                    android:visibility="@{model.iCalObjectUpdated.dtstart != null ? View.VISIBLE : View.GONE}"
                    tools:text="December" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_started_year"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.convertLongToYearString(model.iCalObjectUpdated.dtstart, model.iCalObjectUpdated.dtstartTimezone)}"
                    android:textAlignment="center"
                    android:textSize="12sp"
                    android:textStyle="bold"
                    android:visibility="@{model.iCalObjectUpdated.dtstart != null ? View.VISIBLE : View.GONE}"
                    tools:text="2020" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_started_time"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.convertLongToTimeString(model.iCalObjectUpdated.dtstart, model.iCalObjectUpdated.dtstartTimezone)}"
                    android:textAlignment="center"
                    android:textSize="18sp"
                    android:textStyle="bold"
                    android:visibility="@{model.addTimeChecked ? View.VISIBLE : View.GONE}"
                    tools:text="13:30" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_started_timezone_offset"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.getOffsetStringFromTimezone(model.iCalObjectUpdated.dtstartTimezone)}"
                    android:textAlignment="center"
                    android:textSize="12sp"
                    android:textStyle="bold"
                    android:visibility="@{model.addTimezoneTodoChecked ? View.VISIBLE : View.GONE}"
                    tools:text="GMT+1" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_started_timezone_name"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{model.iCalObjectUpdated.dtstartTimezone}"
                    android:textAlignment="center"
                    android:textSize="12sp"
                    android:visibility="@{model.addTimezoneTodoChecked ? View.VISIBLE : View.GONE}"
                    tools:text="Europe/Vienna" />

                <ImageView
                    android:id="@+id/edit_task_started_add"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_margin="8dp"
                    android:visibility="@{model.iCalObjectUpdated.dtstart == null ? View.VISIBLE : View.GONE}"
                    app:srcCompat="@drawable/ic_date"
                    android:contentDescription="@string/edit_task_started_add" />
            </LinearLayout>
        </com.google.android.material.card.MaterialCardView>


        <com.google.android.material.textview.MaterialTextView
            android:id="@+id/edit_task_due_header"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/due"
            android:textAlignment="center"
            android:textStyle="bold"
            android:visibility="@{model.duedateVisible ? View.VISIBLE : View.GONE}"
            app:layout_constraintEnd_toEndOf="@+id/edit_task_due_card"
            app:layout_constraintStart_toStartOf="@+id/edit_task_due_card"
            app:layout_constraintTop_toBottomOf="@id/edit_task_add_time_switch" />

        <com.google.android.material.card.MaterialCardView
            android:id="@+id/edit_task_due_card"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="16dp"
            android:visibility="@{model.duedateVisible ? View.VISIBLE : View.GONE}"
            app:layout_constraintEnd_toStartOf="@+id/edit_task_completed_card"
            app:layout_constraintHorizontal_bias="0.5"
            app:layout_constraintStart_toEndOf="@+id/edit_task_started_card"
            app:layout_constraintTop_toBottomOf="@+id/edit_task_due_header">

            <LinearLayout
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_horizontal"
                android:orientation="vertical"
                android:paddingStart="4dp"
                android:paddingEnd="4dp"
                android:paddingBottom="8dp">

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_due_day"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.convertLongToDayString(model.iCalObjectUpdated.due, model.iCalObjectUpdated.dueTimezone)}"
                    android:textAlignment="center"
                    android:textSize="36sp"
                    android:textStyle="bold"
                    android:visibility="@{model.iCalObjectUpdated.due != null ? View.VISIBLE : View.GONE}"
                    tools:text="31" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_due_month"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.convertLongToMonthString(model.iCalObjectUpdated.due, model.iCalObjectUpdated.dueTimezone)}"
                    android:textAlignment="center"
                    android:textSize="12sp"
                    android:textStyle="bold"
                    android:visibility="@{model.iCalObjectUpdated.due != null ? View.VISIBLE : View.GONE}"
                    tools:text="December" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_due_year"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.convertLongToYearString(model.iCalObjectUpdated.due, model.iCalObjectUpdated.dueTimezone)}"
                    android:textAlignment="center"
                    android:textSize="12sp"
                    android:textStyle="bold"
                    android:visibility="@{model.iCalObjectUpdated.due != null ? View.VISIBLE : View.GONE}"
                    tools:text="2020" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_due_time"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.convertLongToTimeString(model.iCalObjectUpdated.due, model.iCalObjectUpdated.dueTimezone)}"
                    android:textAlignment="center"
                    android:textSize="18sp"
                    android:textStyle="bold"
                    android:visibility="@{model.addTimeChecked ? View.VISIBLE : View.GONE}"
                    tools:text="13:30" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_due_timezone_offset"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.getOffsetStringFromTimezone(model.iCalObjectUpdated.dueTimezone)}"
                    android:textAlignment="center"
                    android:textSize="12sp"
                    android:textStyle="bold"
                    android:visibility="@{model.addTimezoneTodoChecked ? View.VISIBLE : View.GONE}"
                    tools:text="GMT+1" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_due_timezone_name"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{model.iCalObjectUpdated.dueTimezone}"
                    android:textAlignment="center"
                    android:textSize="12sp"
                    android:visibility="@{model.addTimezoneTodoChecked ? View.VISIBLE : View.GONE}"
                    tools:text="Europe/Vienna" />

                <ImageView
                    android:id="@+id/edit_task_due_add"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_margin="8dp"
                    android:visibility="@{model.iCalObjectUpdated.due == null ? View.VISIBLE : View.GONE}"
                    app:srcCompat="@drawable/ic_date"
                    android:contentDescription="@string/edit_task_due_add" />
            </LinearLayout>
        </com.google.android.material.card.MaterialCardView>

        <com.google.android.material.textview.MaterialTextView
            android:id="@+id/edit_task_completed_header"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@string/completed"
            android:textAlignment="center"
            android:textStyle="bold"
            android:visibility="@{model.completeddateVisible ? View.VISIBLE : View.GONE}"
            app:layout_constraintEnd_toEndOf="@+id/edit_task_completed_card"
            app:layout_constraintStart_toStartOf="@+id/edit_task_completed_card"
            app:layout_constraintTop_toBottomOf="@id/edit_task_add_time_switch" />

        <com.google.android.material.card.MaterialCardView
            android:id="@+id/edit_task_completed_card"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="16dp"
            android:layout_marginEnd="16dp"
            android:visibility="@{model.completeddateVisible ? View.VISIBLE : View.GONE}"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintHorizontal_bias="0.5"
            app:layout_constraintStart_toEndOf="@+id/edit_task_due_card"
            app:layout_constraintTop_toBottomOf="@+id/edit_task_completed_header">

            <LinearLayout
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_horizontal"
                android:orientation="vertical"
                android:paddingStart="4dp"
                android:paddingEnd="4dp"
                android:paddingBottom="8dp">

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_completed_day"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.convertLongToDayString(model.iCalObjectUpdated.completed, model.iCalObjectUpdated.completedTimezone)}"
                    android:textAlignment="center"
                    android:textSize="36sp"
                    android:textStyle="bold"
                    android:visibility="@{model.iCalObjectUpdated.completed != null ? View.VISIBLE : View.GONE}"
                    tools:text="31" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_completed_month"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.convertLongToMonthString(model.iCalObjectUpdated.completed, model.iCalObjectUpdated.completedTimezone)}"
                    android:textAlignment="center"
                    android:textSize="12sp"
                    android:textStyle="bold"
                    android:visibility="@{model.iCalObjectUpdated.completed != null ? View.VISIBLE : View.GONE}"
                    tools:text="December" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_completed_year"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.convertLongToYearString(model.iCalObjectUpdated.completed, model.iCalObjectUpdated.completedTimezone)}"
                    android:textAlignment="center"
                    android:textSize="12sp"
                    android:textStyle="bold"
                    android:visibility="@{model.iCalObjectUpdated.completed != null ? View.VISIBLE : View.GONE}"
                    tools:text="2020" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_completed_time"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.convertLongToTimeString(model.iCalObjectUpdated.completed, model.iCalObjectUpdated.completedTimezone)}"
                    android:textAlignment="center"
                    android:textSize="18sp"
                    android:textStyle="bold"
                    android:visibility="@{model.addTimeChecked ? View.VISIBLE : View.GONE}"
                    tools:text="13:30" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_completed_timezone_offset"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{DateTimeUtils.INSTANCE.getOffsetStringFromTimezone(model.iCalObjectUpdated.completedTimezone)}"
                    android:textAlignment="center"
                    android:textSize="12sp"
                    android:textStyle="bold"
                    android:visibility="@{model.addTimezoneTodoChecked ? View.VISIBLE : View.GONE}"
                    tools:text="GMT+1" />

                <com.google.android.material.textview.MaterialTextView
                    android:id="@+id/edit_task_completed_timezone_name"
                    android:layout_width="80dp"
                    android:layout_height="wrap_content"
                    android:text="@{model.iCalObjectUpdated.completedTimezone}"
                    android:textAlignment="center"
                    android:textSize="12sp"
                    android:visibility="@{model.addTimezoneTodoChecked ? View.VISIBLE : View.GONE}"
                    tools:text="Europe/Vienna" />

                <ImageView
                    android:id="@+id/edit_task_completed_add"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_margin="8dp"
                    android:visibility="@{model.iCalObjectUpdated.completed == null ? View.VISIBLE : View.GONE}"
                    app:srcCompat="@drawable/ic_date"
                    android:contentDescription="@string/edit_task_completed_add" />
            </LinearLayout>
        </com.google.android.material.card.MaterialCardView>


    </androidx.constraintlayout.widget.ConstraintLayout>

</layout>